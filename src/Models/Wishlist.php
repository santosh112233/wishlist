<?php

namespace Santosh\Wishlist\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{

    // protected table = config()
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(config('Wishlist.product_model'),'product_id');
    }

    public function scopeOfProduct($query, $product_id)
    {
        return $query->where('product_id', $product_id);
    }
    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

}

<?php

namespace Santosh\Wishlist;

use Illuminate\Support\ServiceProvider;
use Santosh\Wishlist\Interfaces\WishlistInterface;
use Santosh\Wishlist\Repositories\WishListRepository;

class WishlistServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishMigrations();
        $this->publishConfiguration();
    }
    public function register()
    {
        $this->app->bind('wishlist', Wishlist::class);
        $this->app->bind(WishlistInterface::class, WishListRepository::class);
    }

    public function publishConfiguration()
    {
        $path   =   realpath(__DIR__.'/Config/Wishlist.php');
        $this->publishes([$path => config_path('Wishlist.php')], 'config');
    }
    public function publishMigrations()
    {
        $this->publishes([
            __DIR__.'/Database/Migrations/' => database_path('/migrations')
        ], 'migrations');
    }
}

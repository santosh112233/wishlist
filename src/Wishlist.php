<?php

namespace Santosh\Wishlist;

use Santosh\Wishlist\Interfaces\WishlistInterface;

class Wishlist
{

    public function __construct(WishlistInterface $wishlistInterface)
    {
        $this->wishlistInterface = $wishlistInterface;
    }
    public function addItem($product_id, $user_id)
    {
      return  $this->wishlistInterface->add($product_id, $user_id);
    }
    public function getItem( $user_id)
    {
      return  $this->wishlistInterface->list( $user_id);
    }
    public function removeByProduct($product_id, $user_id)
    {
      return $this->wishlistInterface->removeByProduct($product_id, $user_id);
    }

    public function removeItem($product_id, $user_id)
    {
      return  $this->wishlistInterface->remove($product_id, $user_id);
    }
    public function count($user_id)
    {
        return $this->wishlistInterface->count($user_id);
    }

}

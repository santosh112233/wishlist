<?php
 namespace Santosh\Wishlist\Repositories;

use Santosh\Wishlist\Interfaces\WishlistInterface;
use Santosh\Wishlist\Models\Wishlist;

class WishListRepository implements WishlistInterface
{
    public $wishList;
    public function __construct(Wishlist $wishlist)
    {
        $this->wishList = $wishlist;
    }
    public function add($product_id, $user_id)
    {
        $this->update($product_id, $user_id);
    }

    public function list($user_id)
    {
        return $this->wishList->ofUser($user_id)->get();
    }

    public function update ($product_id, $user_id)
    {
        $wishlist = $this->wishList->ofUser($user_id)->ofProduct($product_id)->first();
        if(is_null($wishlist))
        {
           return  $this->wishList->create([
                'user_id' => $user_id,
                'product_id' => $product_id,
            ]);
        }
    }

    public function remove($id)
    {
        $this->wishList->findOrFail($id)->delete();
    }

    public function removeByProduct($product_id, $user_id)
    {
        $wishlist = $this->wishList->ofUser($user_id)->ofProduct($product_id)->first();
        if(!is_null($wishlist))
        {
        return    $wishlist->delete();
        }
    }

    public function count($user_id)
    {
      return  $this->wishList->ofuser($user_id)->count();
    }

}

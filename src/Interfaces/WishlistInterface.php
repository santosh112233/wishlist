<?php

namespace Santosh\Wishlist\Interfaces ;

interface WishlistInterface
{
    public function add($product_id, $user_id);
    public function list($user_id);
    public function update($product_id, $user_id);
    public function remove($id);
    public function count($user_id);

}

<nav id="navbar">
  <header>WISHLIST Documentation</header>
  <ul>
    <li><a class="nav-link" href="#Introduction">Introduction</a></li>
    <li><a class="nav-link" href="#installation">Installation</a></li>
    <li><a class="nav-link" href="#example">Basic Example</a></li>
       <li><a class="nav-link" href="#config">Configuration Basic Example</a></li>
    <li>
      <a class="nav-link" href="#Declaring_variables">Code Sample</a>
    </li>
   
  </ul>
</nav>
<main id="main-doc">
  <section class="main-section" id="Introduction">
    <header>Introduction</header>
    <article>
      <p>
        Wishlist package is intended to Store a record of future purchase plan product or item .
      </p>
      
    </article>
  </section>


  <section class="main-section" id="installation">
    <header>Hello world</header>
    <article>
      To Install this package you must have php version  > 7 and laravel 6.* >
      <code>
       	composer require santoshghimire/wishlist
      </code>
      	Publish a configuration and migrations file from vendor to root
      <code>
       	php artisan vendor:publish --tag=migrations //for migration file
       	php artisan vendor:publish --tag=config //for configuration file
      </code>
    </article>
  </section>
  <section class="main-section" id="example">
    <header>Examples </header>
    <article>
    <p>
      Some Of Example Of wishlist are
    </p>
    <code>
    	Wishlist::addItem($product_id, $user_id);
    	Wishlist::getItem( $user_id);
      Wishlist::count();
    	Wishlist::removeItem( $user_id);
      Wishlist::removeByProduct($product_id, $user_id);
      
      

    </code>
    	
    </article>
  

  </section>
    <section class="main-section" id="config">
    <header>Configuration </header>
    <article>
      <p>
        You can define properties in config file
      </p>

      	return 
      	[
      	'model' => 'product', // this may change
      	]
  
      
    </article>
  </section>



</main> 
